using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
 
using mvc.Models;
using MongoDB.Bson;
 
namespace mvc.Controllers
{
    [Route("api/Log")]
    public class LogAPIController : Controller
    {
        DataAccess objds;
 
        public LogAPIController(DataAccess d)
        {
            objds = d; 
        }
 
        [HttpGet]
        public IEnumerable<Log> Get()
        {
            return objds.GetLogs();
        }
        [HttpGet("{id:length(24)}")]
        public IActionResult Get(string id)
        {
            var Log = objds.GetLog(new ObjectId(id));
            if (Log == null)
            {
                return NotFound();
            }
            return new ObjectResult(Log);
        }
 
        [HttpPost]
        public IActionResult Post([FromBody]Log p)
        {
            objds.Create(p);
            return new OkObjectResult(p);
        }
        [HttpPut("{id:length(24)}")]
        public IActionResult Put(string id, [FromBody]Log p)
        {
            var recId = new ObjectId(id);
            var Log = objds.GetLog(recId);
            if (Log == null)
            {
                return NotFound();
            }
            
            objds.Update(recId, p);
            return new OkResult();
        }
 
        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var Log = objds.GetLog(new ObjectId(id));
            if (Log == null)
            {
                return NotFound();
            }
 
            objds.Remove(Log.Id);
            return new OkResult();
        }
    }
}