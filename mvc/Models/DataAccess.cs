using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
 
namespace mvc.Models
{
    public class DataAccess
    {
        MongoClient _client;
        MongoServer _server;
        MongoDatabase _db;
 
        public DataAccess()
        {
            _client = new MongoClient("mongodb://localhost:27017");
            _server = _client.GetServer();
            _db = _server.GetDatabase("LogDB");      
        }
 
        public IEnumerable<Log> GetLogs()
        {
            return _db.GetCollection<Log>("Logs").FindAll();
        }
 
 
        public Log GetLog(ObjectId id)
        {
            var res = Query<Log>.EQ(p=>p.Id,id);
            return _db.GetCollection<Log>("Logs").FindOne(res);
        }
 
        public Log Create(Log p)
        {
            _db.GetCollection<Log>("Logs").Save(p);
            return p;
        }
 
        public void Update(ObjectId id,Log p)
        {
            p.Id = id;
            var res = Query<Log>.EQ(pd => pd.Id,id);
            var operation = Update<Log>.Replace(p);
            _db.GetCollection<Log>("Logs").Update(res,operation);
        }
        public void Remove(ObjectId id)
        {
            var res = Query<Log>.EQ(e => e.Id, id);
            var operation = _db.GetCollection<Log>("Logs").Remove(res);
        }
    }
}