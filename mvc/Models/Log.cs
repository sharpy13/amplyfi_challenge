using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace mvc.Models
{
    public class Log
    {
        public ObjectId Id { get; set; }
        [BsonElement("m_szDocID")]
        public string DocId { get; set;}
        [BsonElement("m_szDocTitle")]
        public string DocTitle { get; set;}
        [BsonElement("m_szYear")]
        public string Year { get; set;}
        [BsonElement("m_szDocSumamry")]
        public string DocSummary { get; set;}
        [BsonElement("m_szDocBody")]
        public string DocBody { get; set;}
        [BsonElement("m_szGeo1")]
        public string Geo1 { get; set;}
        [BsonElement("m_szSourceType")]
        public string SourceType { get; set;}
        [BsonElement("m_szSrcUrl")]
        public string SrcUrl { get; set;}
        [BsonElement("m_Places")]
        public string[] Places { get; set;}
        [BsonElement("m_People")]
        public string[] People { get; set;}
        [BsonElement("m_Companies")]
        public string[] Companies { get; set;}
        [BsonElement("m_BiGrams")]
        public string[] BiGrams { get; set;}
        [BsonElement("m_TriGrams")]
        public string[] TriGrams { get; set;}
        [BsonElement("m_SocialTags")]
        public string[] SocialTags { get; set;}
        [BsonElement("m_Topics")]
        public string[] Topics { get; set;}
        [BsonElement("m_Industry")]
        public string[] Industry { get; set;}
        [BsonElement("m_Technology")]
        public string[] Technology { get; set;}
        [BsonElement("m_BiCnt")]
        public int[] BiCnt { get; set;}
        [BsonElement("m_TriCnt")]
        public int[] TriCnt { get; set;}
        [BsonElement("m_iDocBodyWordCnt")]
        public int DocBodyWordCnt { get; set;}
    }
}