queue()
    .defer(d3.json, "/electric_cars/standard_logs")
    .defer(d3.json, "static/geojson/world.json")
    .await(makeGraphs);

function makeGraphs(error, logsJson, countriesJson) {
	
	//Clean logsJson data
	var electriccarsLogs = logsJson;
	var dateFormat = d3.time.format("%Y");
	electriccarsLogs.forEach(function(d) {
		d["m_szYear"] = dateFormat.parse(d["m_szYear"]);
		d["m_szYear"].setDate(1);
	});

	//Create a Crossfilter instance
	var ndx = crossfilter(electriccarsLogs);

	//Define Dimensions
	var dateDim = ndx.dimension(function(d) { return d["m_szYear"]; });
	var sourceTypeDim = ndx.dimension(function(d) { return d["m_szSourceType"]; });
	var stateDim = ndx.dimension(function(d) { return d["m_szGeo1"]; });
	var totalLogsDim  = ndx.dimension(function(d) { return 1; });


	//Calculate metrics
	var numLogsByDate = dateDim.group(); 
	var numLogsBySourceType = sourceTypeDim.group();
    var totalLogsByState = stateDim.group().reduceSum(function(d) {
		return 1;
	});

	var all = ndx.groupAll();
	var totalLogs = ndx.groupAll().reduceSum(function(d) {return 1;});

	//Define values (to be used in charts)
	var max_state = totalLogsByState.top(1)[0].value;
	var minDate = dateDim.bottom(1)[0]["m_szYear"];
	var maxDate = dateDim.top(1)[0]["m_szYear"];

    //Charts
	var timeChart = dc.barChart("#time-chart");
	var sourceTypeChart = dc.rowChart("#source-type-row-chart");
	var worldChart = dc.geoChoroplethChart("#world-chart");
	var totalLogsND = dc.numberDisplay("#total-Logs-nd");


	totalLogsND
		.formatNumber(d3.format("d"))
		.valueAccessor(function(d){return d; })
		.group(totalLogs)
		.formatNumber(d3.format(".3s"));

	timeChart
		.width(900)
		.height(160)
		.margins({top: 10, right: 50, bottom: 30, left: 50})
		.dimension(dateDim)
		.group(numLogsByDate)
		.transitionDuration(500)
		.x(d3.time.scale().domain([minDate, maxDate]))
		.elasticY(true)
		.xAxisLabel("Year")
		.yAxis().ticks(4);

	sourceTypeChart
        .width(900)
        .height(250)
        .dimension(sourceTypeDim)
        .group(numLogsBySourceType)
        .xAxis().ticks(4);

	worldChart.width(920)
		.height(350)
		.dimension(stateDim)
		.group(totalLogsByState)
		.colors(["#E2F2FF", "#C4E4FF", "#9ED2FF", "#81C5FF", "#6BBAFF", "#51AEFF", "#36A2FF", "#1E96FF", "#0089FF", "#0061B5"])
		.colorDomain([0, max_state])
		.overlayGeoJson(countriesJson["features"], "state", function (d) {
			return d.properties.name;
		})
		.projection(d3.geo.equirectangular()
    				.scale(150)
					.translate([480, 200]))
		.title(function (p) {
			return "State: " + p["key"]
					+ "\n"
					+ "Total Logs: " + Math.round(p["value"]);
		})

    dc.renderAll();
};