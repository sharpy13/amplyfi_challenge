from gensim import corpora, models, similarities
from pymongo import MongoClient
from mpi4py import MPI
import multiprocessing
import sys

# Constants
THIS_WORKER = MPI.COMM_WORLD.Get_rank()
MASTER_WORKER = 0
SLAVE_WORKERS = range(MPI.COMM_WORLD.Get_size())
SLAVE_WORKERS.remove(MASTER_WORKER)
MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
DBS_NAME = 'electric_cars'
COLLECTION_NAME = 'standard_logs'
NEW_COLLECTION_NAME = 'cleaned_logs'
FIELDS = {'_id': True}
SIGNIFICANCE_THRESHOLD = 0.5 # Higher SIGNIFICANCE_THRESHOLD will keep only most significant ngrams while lower SIGNIFICANCE_THRESHOLD will keep less significant ngrams


# Here we load our LSI model
print "Worker {} loading model".format(THIS_WORKER)
dictionary = corpora.Dictionary.load("electric_cars.dictionary")
model = models.LsiModel.load("lsi.model")
index = similarities.MatrixSimilarity.load("lsi.index")
print "Worker {} loaded model".format(THIS_WORKER)


# Method for deciding which ngrams to keep and which to delete
def clean_ngram(ngram):
    vec_bow = dictionary.doc2bow(ngram.lower().split())
    vec_model = model[vec_bow]
    sims = index[vec_model]
    signigicance_measure = max(sims)
    if signigicance_measure < SIGNIFICANCE_THRESHOLD:
        return True
    else:
        return False

# Here we divide the processing for multiple workers
connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
collection = connection[DBS_NAME][COLLECTION_NAME]

number_of_documents = collection.count()
number_of_cpu_cores = multiprocessing.cpu_count()
batch_of_documents = number_of_documents / number_of_cpu_cores

documents_workload = []
batch = []
all_documents = collection.find(projection=FIELDS)

for i, document in enumerate(all_documents):
    batch.append(document["_id"])
    if (i + 1) % batch_of_documents == 0:
        documents_workload.append(batch)
        batch = []
    elif (i + 1) == number_of_documents:
        documents_workload.append(batch)

# Here is the parallel part
cleaned_logs = []
portion_of_documents = collection.find({ '_id' : { '$in' : documents_workload[THIS_WORKER] } })

for i, retrieved_log in enumerate(portion_of_documents):
    
    print "Worker {} processing log {}".format(THIS_WORKER, retrieved_log["m_szDocID"])

    new_log = {"m_szDocID": "",
               "m_szDocTitle": "",
               "m_szYear": "",
               "m_szGeo1": "",
               "m_szSourceType": "",
               "m_BiGrams": {"Keep": [],
                            "Delete": []    
                           },
               "m_TriGrams": {"Keep": [],
                             "Delete": []    
                            }
              }
    
    new_log["m_szDocID"] = retrieved_log["m_szDocID"]
    new_log["m_szDocTitle"] = retrieved_log["m_szDocTitle"]
    new_log["m_szYear"] = retrieved_log["m_szYear"]
    new_log["m_szGeo1"] = retrieved_log["m_szGeo1"]
    new_log["m_szSourceType"] = retrieved_log["m_szSourceType"]
    
    bigrams = retrieved_log["m_BiGrams"]
    trigrams = retrieved_log["m_TriGrams"]
        
    for bigram in bigrams:
        if clean_ngram(bigram):
            new_log["m_BiGrams"]["Delete"].append(bigram)
        else:
            new_log["m_BiGrams"]["Keep"].append(bigram)
    
    for trigram in trigrams:
        if clean_ngram(trigram):
            new_log["m_TriGrams"]["Delete"].append(trigram)
        else:
            new_log["m_TriGrams"]["Keep"].append(trigram)
    
    cleaned_logs.append(new_log)
    if i == 3:
        break
    print "Worker {} processed log {}".format(THIS_WORKER, retrieved_log["m_szDocID"])

# When processing is done all slave workers will send their processed logs to master worker
if THIS_WORKER != MASTER_WORKER:
    print "Worker {} sending cleaned logs to main worker".format(THIS_WORKER)
    MPI.COMM_WORLD.send(cleaned_logs, dest=MASTER_WORKER, tag=1)
    print "Worker {} stopping".format(THIS_WORKER)
    sys.exit(0)

# Master worker receives and adds all of the processed logs and inserts them into mongo database "electric_cars" under collection "cleaned_logs"
for slave in SLAVE_WORKERS:
    cleaned_logs += MPI.COMM_WORLD.recv(source=slave, tag=1)

print "Worker {} inserting  cleaned logs into Mongo DB".format(THIS_WORKER)

collection = connection[DBS_NAME][NEW_COLLECTION_NAME]
collection.insert_many(cleaned_logs)

print "Worker {} stopping".format(THIS_WORKER)
sys.exit(0)
