from flask import Flask
from flask import render_template
from pymongo import MongoClient
from bson import json_util
from bson.json_util import dumps
import json

# Run the server
app = Flask(__name__)

MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
DBS_NAME = 'electric_cars'
COLLECTION_NAME = 'standard_logs'
FIELDS = {'m_szYear': True, 'm_szGeo1': True, 'm_szSourceType': True, 'm_iDocBodyWordCnt': True, '_id': False}

# Main page
@app.route("/")
def index():
    return render_template("index.html")

# Load data from mongo db
@app.route("/electric_cars/standard_logs")
def electric_cars_standard_logs():
    connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
    collection = connection[DBS_NAME][COLLECTION_NAME]
    logs = collection.find(projection=FIELDS, limit=100000)
    json_logs = []
    for log in logs:
        json_logs.append(log)
    json_logs = json.dumps(json_logs, default=json_util.default)
    connection.close()
    return json_logs

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000,debug=True)