# README #

This repo contains contains solution for AMPLYFI challenge.

### What is this repository for? ###

* Data mining of JSON files containing interesting information
* ver 0.1.0

### Requirements ###
* Docker 17.03.1-ce
* Docker Compose 1.13.0

### How do I get set up? ###

* pull this repo
* cd into the repo folder
* add all your JSONs into JSON folder (do not remove shell script import_logs.sh)
* run "docker-compose up" (takes some minutes)
* in another terminal window run "docker exec -it mongodb bash"
* cd into folder /home/data
* run command "./import_logs.sh" (takes some minutes, this will import all of the JSON data into mongo db)
* connect to 0.0.0.0:5000 through your web browser (now you can do visualizations and queries)

### Things to fix ###

* set up the application container so that it is possible to use python gensim

### Who do I talk to? ###

* Roman Kopšo kopsor [at] gmail dot com
