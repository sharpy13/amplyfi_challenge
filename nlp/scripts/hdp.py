from gensim import corpora, models, similarities
from gensim.parsing.preprocessing import STOPWORDS
from gensim.utils import simple_preprocess
from collections import defaultdict
from pymongo import MongoClient
import os.path

# Parameters to connect with our mongo db
MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
DBS_NAME = 'electric_cars'
COLLECTION_NAME = 'standard_logs'
FIELDS = {'m_szDocBody': True, '_id': False}

# Connect to mongo to retrieve all doc content
connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
collection = connection[DBS_NAME][COLLECTION_NAME]

# Tokenize helper function
def tokenize(text):
    return [token for token in simple_preprocess(text) if token not in STOPWORDS]

# Python memory friendly iterator over processed and tokenized doc contents
class Corpus:
    def __iter__(self):
        for log in collection.find(projection=FIELDS):
            yield tokenize(log["m_szDocBody"])

# Count the frequencies of the words
print "*** COUNTING THE FREQUENCY OF WORDS *** \n"
friendly_corpus = Corpus()
frequency = defaultdict(int)
for text in friendly_corpus:
    for token in text:
        frequency[token] += 1

# Create generator of words from doc bodies that have freqeuncy more than 1
print "*** CREATING GENERATOR OF WORDS WITH FREQUENCY MORE THAN 1 *** \n"
texts = ([token for token in text if frequency[token] > 1]
        for text in friendly_corpus)

# Create dictionary from our texts generator
print "*** CREATING DICTIONARY *** \n"
if os.path.isfile("electric_cars.dictionary"):
    dictionary = corpora.Dictionary.load("electric_cars.dictionary")
else:
    dictionary = corpora.Dictionary(texts)
    
    # Reset texts generator
    print "*** RESETING GENERATOR OF WORDS WITH FREQUENCY MORE THAN 1 *** \n"
    texts = ([token for token in text if frequency[token] > 1]
            for text in friendly_corpus)

# Create text corpus
print "*** CREATING CORPUS *** \n"
corpus = [dictionary.doc2bow(text) for text in texts]

# Prepare our tfidf mode and tfidf index
print "*** PREPARING HDP MODEL AND INDEX  *** \n"
hdp = models.HdpModel(corpus, id2word=dictionary)
index = similarities.MatrixSimilarity(hdp[corpus])

# Save the dictionary, model and index into the current folder
print "*** SAVING OUTPUT FILES *** \n"
if not os.path.exists("hdp"):
    os.makedirs("hdp")
hdp.save("hdp/hdp.model")
index.save("hdp/hdp.index")
if not os.path.isfile("electric_cars.dictionary"):
    dictionary.save("electric_cars.dictionary")
